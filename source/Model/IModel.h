#pragma once
#include <windows.h>
#include "GL/glew.h"
#include <GL/GL.h>
#include <GL/GLU.h>
#include <string>
#include "GLSLShaderData.h"

/** 
*	Every renderable object inherits from this interface for simplicity
*/
class IModel
{
public:
	// Methods
	IModel(const std::string& sVSFileName, const std::string& sFSFileName, 
            const std::string& sDepthVSFileName, const std::string& sDepthFSFileName)
		: m_shader(sVSFileName, sFSFileName),
          m_depthShader(sDepthVSFileName, sDepthFSFileName), 
          m_texture(0)
	{
		m_rotX = m_rotY = m_rotZ = 0;
		m_posX = m_posY = m_posZ = 0;

		m_worldMatrixID = glGetUniformLocationARB(m_shader.m_programHandler,"WorldMatrix");
		m_textureID = glGetUniformLocationARB(m_shader.m_programHandler,"tDiffuse");

        glUseProgram(m_depthShader.m_programHandler);

        GLint infoLogLength;
        glGetShaderiv(m_depthShader.m_vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar * strInfoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(m_depthShader.m_vertexShader, infoLogLength, NULL, strInfoLog);

        GLint uniforms;
        glGetProgramiv(m_depthShader.m_programHandler, GL_ACTIVE_UNIFORMS, &uniforms);

        for (int i = 0; i<uniforms; ++i) {
            int name_len = -1, num = -1;
            GLenum type = GL_ZERO;
            char name[100];
            glGetActiveUniform(m_depthShader.m_programHandler, GLuint(i), sizeof(name) - 1,
                &name_len, &num, &type, name);
            name[name_len] = 0;
            GLuint location = glGetUniformLocation(m_depthShader.m_programHandler, name);
        }

		m_depthWorldMatrixID = glGetUniformLocationARB(m_depthShader.m_programHandler, "WorldMatrix");
        m_depthTextureID = glGetUniformLocationARB(m_depthShader.m_programHandler, "tDiffuse");
	}

	virtual ~IModel(){}

	bool			loadTexture(const std::string& textureName);
	void			setPosition(float x, float y, float z);
	void			setRotation(float x, float y, float z);
	void			addRotation(float x, float y, float z);
	virtual void	render() const = 0;
    virtual void    shadowRender() const = 0;

public:
	// Fields
	GLSLShaderData 	m_shader; // Every model must have a shader associated (both vertex and fragment)
    GLSLShaderData 	m_depthShader;

	GLuint			m_worldMatrixID; // This ID is used to pass the world matrix into the shader
	float			m_rotX, m_rotY, m_rotZ; // Rotations
	float			m_posX, m_posY, m_posZ; // Positions
	
	GLuint			m_textureID; // Texture ID used to pass the texture into the shader
	GLuint			m_texture; // OpenGL texture ID

	GLuint			m_depthWorldMatrixID; // This ID is used to pass the world matrix into the shader
	GLuint			m_depthTextureID; // Texture ID used to pass the texture into the shader

    GLuint          m_worldToLightViewMatrix_shaderID; // Shader ID for the specified matrix
    GLuint          m_lightViewToProjectionMatrix_shaderID; // Shader ID for the specified matrix
    GLuint          m_worldToCameraViewMatrix_shaderID; // Shader ID for the specified matrix
};

