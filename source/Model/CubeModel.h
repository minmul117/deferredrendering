#pragma once
#include "IModel.h"

/** 
*	A simple cube model that is easy to render
*/
class CubeModel : public IModel
{
public:
	// Methods
	CubeModel(const std::string& sVSFileName, const std::string& sFSFileName,
        const std::string& sDepthVSFileName, const std::string& sDepthFSFileName, float side);

	void	render() const;
    void    shadowRender() const;

protected:
	// Fields
	float m_side;
};

