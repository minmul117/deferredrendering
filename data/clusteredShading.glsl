#ifndef _CLUSTERED_SHADING_GLSL_
#define _CLUSTERED_SHADING_GLSL_
/****************************************************************************/
/* Copyright (c) 2011, Ola Olsson, Ulf Assarsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#include "globals.glsl"

/**
 * This file contains functions and declarations needed to conventiently implement
 * clustered (deferred or forward) shading in a shader.
 */
uniform isamplerBuffer clusterLightIndexListsTex;
uniform usamplerBuffer clusterGridTex;

uniform LightPositionsRanges
{
  vec4 g_light_position_range[NUM_POSSIBLE_LIGHTS];
};
uniform LightColors
{
  vec4 g_light_color[NUM_POSSIBLE_LIGHTS];
};

/**
 * Computes contributions from a point light with a linear falloff using a blinn-phong model.
 */
vec3 doLight(vec3 position, vec3 normal, vec3 diffuse, vec3 specular, float shininess, vec3 viewDir, vec3 lightPos, vec3 lightColor, float range)
{
  vec3 lightDir = vec3(lightPos) - position;
  float dist = length(lightDir);
  lightDir = normalize(lightDir);
  float inner = 0.0;

  float ndotL = max(dot(normal, lightDir),0.0);
  float att = max(1.0 - max(0.0, (dist - inner) / (range - inner)), 0.0);

	vec3 h = normalize(lightDir + viewDir);

	float normalizationFactor = ((shininess + 2.0) / 8.0);

	vec3 spec = specular * pow(max(0, dot(h, normal)), shininess) * normalizationFactor;

  return ndotL * att * lightColor * (diffuse + spec);
}


/**
 * Looks up light parameters in constant buffers using light index, and 
 * computes lighting by invoking doLight with these.
 */
vec3 doLight(vec3 position, vec3 normal, vec3 diffuse, vec3 specular, float shininess, vec3 viewDir, int lightIndex)
{
  vec3 lightPos = g_light_position_range[lightIndex].xyz;
  float lightRange = g_light_position_range[lightIndex].w;
  vec3 lightColor = g_light_color[lightIndex].xyz;

  return doLight(position, normal, diffuse, specular, shininess, viewDir, lightPos, lightColor, lightRange);
}

/**
 * Computes the {i,j,k} integer index into the cluster grid. For details see our paper:
 * 'Clustered Deferred and Forward Shading'
 * http://www.cse.chalmers.se/~olaolss/main_frame.php?contents=publication&id=clustered_shading
 */
ivec3 calcClusterLoc(vec2 fragPos, float viewSpaceZ)
{
	// i and j coordinates are just the same as tiled shading, and based on screen space position.
  ivec2 l = ivec2(int(fragPos.x) / LIGHT_GRID_TILE_DIM_X, int(fragPos.y) / LIGHT_GRID_TILE_DIM_Y);

	// k is based on the log of the view space Z coordinate.
	float gridLocZ = log(-viewSpaceZ * recNear) * recLogSD1;

	return ivec3(l, int(gridLocZ));
}


/**
 * Linearlizes the 3D cluster location into an offset, which can be used as an
 * address into a linear buffer.
 */
int calcClusterOffset(ivec3 clusterLoc)
{
	return (clusterLoc.z * LIGHT_GRID_MAX_DIM_Y + clusterLoc.y) * LIGHT_GRID_MAX_DIM_X + clusterLoc.x;
}

/**
 * Convenience shorthand function, see above...
 */
int calcClusterOffset(vec2 fragPos, float viewSpaceZ)
{
	return calcClusterOffset(calcClusterLoc(fragPos, viewSpaceZ));
}

/**
 * Computes clustered shading for the current fragment, using the built in 
 * gl_FragCoord and the view space Z coordinate to determine the correct 
 * cluster. The position must be in view space (and thus the normal also).
 */
vec3 evalClusteredShading(in vec3 diffuse, in vec3 specular, in float shininess, in vec3 position, in vec3 normal, in vec3 viewDir)
{
	// fetch cluster data (i.e. offset to light indices, and numer of lights) from grid buffer.
	uvec2 offsetCount = texelFetch(clusterGridTex, calcClusterOffset(gl_FragCoord.xy, position.z)).xy; 

  int lightOffset = int(offsetCount.x);
  int lightCount = int(offsetCount.y);

  vec3 shading = vec3(0.0, 0.0, 0.0);
	
  for (int i = 0; i < lightCount; ++i)
  {
		// fetch light index from list of lights for the cluster.
    int lightIndex = texelFetch(clusterLightIndexListsTex, lightOffset + i).x; 
		// compute and accumulate shading.
	  shading += doLight(position, normal, diffuse, specular, shininess, viewDir, lightIndex);
  }

  return shading;
}


#endif // _CLUSTERED_SHADING_GLSL_
