uniform sampler2D tDiffuse; 
uniform sampler2D tPosition;
uniform sampler2D tNormals;
uniform sampler2D tShadowMap;

uniform vec3 cameraPosition;
uniform mat4 worldToLightViewMatrix;
uniform mat4 lightViewToProjectionMatrix;
uniform mat4 worldToCameraViewMatrix;

float texture2DCompare(sampler2D depthMap, vec2 uv, float compare)
{
	float depth = texture2D(depthMap, uv).x;
	return step(compare, depth);
}

float texture2DShadowLerp(sampler2D depths, vec2 size, vec2 uv, float compare)
{
	vec2 texelSize = vec2(1.0) / size;
	vec2 f = fract(uv * size + 0.5);
	vec2 centroidUV = floor(uv * size + 0.5) / size;

	float lb = texture2DCompare(depths, centroidUV + texelSize * vec2(0.0, 0.0), compare);
	float lt = texture2DCompare(depths, centroidUV + texelSize * vec2(0.0, 1.0), compare);
	float rb = texture2DCompare(depths, centroidUV + texelSize * vec2(1.0, 0.0), compare);
	float rt = texture2DCompare(depths, centroidUV + texelSize * vec2(1.0, 1.0), compare);

	float a = mix(lb, lt, f.y);
	float b = mix(rb, rt, f.y);
	float c = mix(a, b, f.x);

	return c;
}

float PCF(sampler2D depths, vec2 size, vec2 uv, float compare)
{
	float result = 0.0;

	for(int x = -4; x <= 4; x++)
	{
		for(int y = -4; y <= 4; y++)
		{
			vec2 off = vec2(x,y) / size;
			result += texture2DCompare(depths, uv + off, compare);
		}
	}

	return result / 60.0;
}

float PCFWithLerp(sampler2D depths, vec2 size, vec2 uv, float compare)
{
	float result = 0.0;

	for(int x = -4; x <= 4; x++)
	{
		for(int y = -4; y <= 4; y++)
		{
			vec2 off = vec2(x,y) / size;
			result += texture2DShadowLerp(depths, size, uv + off, compare);
		}
	}

	return result / 70.0;
}

float linstep(float min, float max, float v)
{
	return clamp((v - min) / (max - min), 0, 1);
}

float VSM(sampler2D depths, vec2 uv, vec2 size, float compare)
{
	const float bias = 0.0001;
	float depthValue = texture2D( depths, uv ) - bias;

	float moment1 = depthValue;
	float moment2 = depthValue * depthValue;

	float dx = dFdx(depthValue);
	float dy = dFdy(depthValue);

	moment2 += 0.25 * (dx * dx + dy * dy);

	vec2 moments = vec2(moment1, moment2);

	if(compare < moment1)
	{
		return 1.0;
	}

	float variance = moments.y - moments.x * moments.x;
	variance = max(variance,0.00002);

	float d = compare - moments.x;
	float p_max = variance / (variance + d * d);

	return linstep(0.2, 1, p_max);  
}

float readShadowMap(vec3 eyeDir)
{
	mat4 cameraViewToWorldMatrix = inverse(worldToCameraViewMatrix);
	mat4 cameraViewToProjectedLightSpace = lightViewToProjectionMatrix * worldToLightViewMatrix * cameraViewToWorldMatrix;
	vec4 projectedEyeDir = cameraViewToProjectedLightSpace * vec4(eyeDir,1);
	projectedEyeDir = projectedEyeDir/projectedEyeDir.w;

	vec2 textureCoordinates = projectedEyeDir.xy * vec2(0.5,0.5) + vec2(0.5,0.5);

	const float bias = 0.0001;
	float depthValue = texture2D( tShadowMap, textureCoordinates ) - bias;

	float cameraDepth = projectedEyeDir.z * 0.5 + 0.5;
	float size = vec2(1024, 1024);

	//float illuminated = texture2DShadowLerp(tShadowMap, size, textureCoordinates, cameraDepth);
	//float illuminated = PCF(tShadowMap, size, textureCoordinates, cameraDepth);
	//float illuminated = PCFWithLerp(tShadowMap, size, textureCoordinates, cameraDepth);
	float illuminated = VSM(tShadowMap, textureCoordinates, size, cameraDepth);

	//return cameraDepth < depthValue;
	return illuminated;
}

void main( void )
{
	// Read the data from the textures
	vec4 image = texture2D( tDiffuse, gl_TexCoord[0].xy );
	vec4 position = texture2D( tPosition, gl_TexCoord[0].xy );
	vec4 normal = texture2D( tNormals, gl_TexCoord[0].xy );
	
	mat4 lightViewToWolrdMatrix = inverse(worldToLightViewMatrix);
	vec3 light = lightViewToWolrdMatrix[3].xyz;
	vec3 lightDir = light - position.xyz;
	
	normal = normalize(normal);
	lightDir = normalize(lightDir);
	
	vec3 eyeDir = position.xyz - cameraPosition;
	vec3 reflectedEyeVector = normalize(reflect(eyeDir, normal));

	float shadow = readShadowMap(eyeDir);

	float diffuseLight = max(dot(normal,lightDir),0) * shadow;
	float ambientLight = 0.1;

	gl_FragColor = (diffuseLight + ambientLight) * image + pow(max(dot(lightDir, reflectedEyeVector),0.0), 100) * 1.5 * shadow;
}
