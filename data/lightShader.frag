varying vec4 position;
varying vec3 normals;

uniform sampler2D tDiffuse;

void main( void )
{
    float depth = position.z / position.w;
    depth = depth * 0.5 + 0.5;
    float moment1 = depth;
    float moment2 = depth * depth;
    float dx = dFdx(depth);
    float dy = dFdy(depth);

    moment2 += 0.25 * (dx * dx + dy * dy);

	float temp = length(normalize(normals));

	gl_FragColor = vec4(moment1 + length(texture2D(tDiffuse,gl_TexCoord[0].st).rgb) * 0.00001, moment2, temp * 0.00001, 1.0);
}